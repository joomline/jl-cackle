<?php
 /**
 * @package com_afisha
 * @author Artem Zhukov (artem@joomline.ru)
 * @version 4.5
 * @copyright (C) 2009-2012 by JoomLine (http://www.joomline.net)
 * @license JoomLine: http://joomline.net/licenzija-joomline.html
 *
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.installer.installer');

$db = &JFactory::getDBO();
$query = "CREATE TABLE IF NOT EXISTS `#__jlcacklepro_comment` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `post_id` varchar(500) DEFAULT NULL,
  `url` varchar(20) DEFAULT NULL,
  `message` text,
  `status` varchar(11) DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `user_agent` varchar(200) NOT NULL,
  `ip` varchar(39) DEFAULT NULL,
  `author_name` varchar(60) DEFAULT NULL,
  `author_email` varchar(100) DEFAULT NULL,
  `author_avatar` varchar(200) DEFAULT NULL,
  `author_www` varchar(200) DEFAULT NULL,
  `author_provider` varchar(32) DEFAULT NULL,
  `anonym_name` varchar(60) DEFAULT NULL,
  `anonym_email` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  UNIQUE KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
$db->setQuery($query);
$db->query();

$query = "CREATE TABLE IF NOT EXISTS `#__jlcacklepro_timer` (
  `time_id` int(11) NOT NULL AUTO_INCREMENT,
  `timevalue` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`time_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
$db->setQuery($query);
$db->query();

$query = "CREATE TABLE IF NOT EXISTS `#__jlcacklepro_mod` (
  `param` varchar(200) NOT NULL DEFAULT '',
  `value` varchar(200) DEFAULT '',
  PRIMARY KEY (`param`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$db->setQuery($query);
$db->query();


?>
